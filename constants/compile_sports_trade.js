module.exports = async (
  mysqlDb,
  sportsTradeId,
  sportsTrade,
  horseDataLoader,
  greyhoundDataLoader,
  teamDataloader
) => {
  let s;
  if (!sportsTrade) {
    const sportTradeColl = await mysqlDb.getCollection("sports_trades");
    console.log(sportTradeColl);
    let s = await sportTradeColl.getOne(sportsTradeId).execute();

    s = s.fetchOne();
  } else {
    s = sportsTrade;
  }

  console.log(s);

  return {
    ...s,
  };
};

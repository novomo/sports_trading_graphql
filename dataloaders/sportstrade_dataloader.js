const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

module.exports.getSportsTradeById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const sportsTradeColl = await mysqlDb.getCollection("sportsTrades");
  let sportsTrade = await sportsTradeColl
    .find("_id = :sportsTradeID")
    .bind("sportsTradeID", id)
    .execute();

  sportsTrade = sportsTrade.fetchOne();
  await session.close();
  return sportsTrade;
};

module.exports.getSportsTradeByIds = async (ids) => {
  return ids.map((id) => getHorseById(id));
};

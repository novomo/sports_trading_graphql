/*
    User resolvers
*/
// resolver functions

const editSportsTrade = require("./resolvers/edit_sports_trade");
const getSportsTrade = require("./resolvers/get_sports_trade");
const newTrade = require("./resolvers/new_trade");
const { pubsub } = require("../../constants/pubsub");
const { withFilter } = require("graphql-subscriptions");
module.exports = {
  Subscription: {
    sportsTradeChange: {
      subscribe: withFilter(
        () => pubsub.asyncIterator(["SPORTS_TRADE_CHANGED"]),
        (payload, variables) => {
          // Only push an update if the comment is on
          // the correct repository for this operation
          //console.log(typeof payload.userChanged._id.toString());
          //console.log(typeof variables.user);
          //console.log(payload.userChanged._id.toString() === variables.user);
          return payload.ChangedTrade.userID.toString() === variables.user;
        }
      ),
    },

    tradeAlert: {
      subscribe: () => pubsub.asyncIterator(["TRADE_ALERT"]),
    },
  },

  Mutation: {
    editSportsTrade,
    newTrade,
  },
  Query: {
    getSportsTrade,
  },
};

const mysqlx = require("@mysql/xdevapi");
const TelegramBot = require("node-telegram-bot-api");
const { toTimestamp } = require("../../../node_normalization/numbers");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
  DISCORD_MESSAGE,
  TELEGRAM_TOKEN,
} = require("../../../env.js");
const { pubsub } = require("../../../constants/pubsub");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
module.exports = async (
  _,
  { inputTradeAlert },
  { currentUser, sportEventDataLoader }
) => {
  let starttime = inputTradeAlert.starttime;
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    let horseRace, greyhoundRace, position, eventID;

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const sportsTradeColl = await mysqlDb.getCollection("sports_trades");
    let trade, stake;
    let message;
    if (inputTradeAlert.sport === "Horse Racing") {
      stake = 0.25;
      const horseRaceColl = await mysqlDb.getCollection("horse_races");
      const courseColl = await mysqlDb.getTable("horse_tracks");
      if (inputTradeAlert.eventID) {
        console.log(horseRaceColl);
        horseRace = await horseRaceColl
          .find("_id like :_id")
          .bind("_id", inputTradeAlert.eventID)
          .execute();

        horseRace = horseRace.fetchOne();
      } else {
        console.log(starttime);
        horseRace = await horseRaceColl
          .find("starttime like :starttime")
          .bind("starttime", starttime)
          .execute();

        horseRace = horseRace.fetchOne();

        if (!horseRace) {
          horseRace = await horseRaceColl
            .find("starttime like :starttime")
            .bind("starttime", starttime + 86400)
            .execute();

          horseRace = horseRace.fetchOne();
          starttime = starttime + 86400;
        }
      }
      console.log(horseRace);
      if (!horseRace) {
        return false;
      }
      let horseCourse = await courseColl
        .select()
        .where(`courseID = ${horseRace.course.courseID}`)
        .execute();
      horseCourse = horseCourse.fetchOne();
      message = `
        Race: ${horseCourse[1]} ${new Date(
        (starttime + 3600) * 1000
      ).toISOString()}
        Strategy: ${inputTradeAlert.strategy}
        Selection: ${inputTradeAlert.selection}
        ${inputTradeAlert.odds ? `Odds: ${inputTradeAlert.odds}` : ""}
    `;

      trade = sportsTradeColl
        .find("eventID like :eventID AND sport like :sport AND userID like 0")
        .bind("eventID", horseRace._id)
        .bind("sport", inputTradeAlert.sport);

      eventID = horseRace._id;
    } else if (inputTradeAlert.sport === "Greyhound Racing") {
      stake = 0.25;
      const greyhoundRaceColl = await mysqlDb.getCollection("greyhound_races");
      if (inputTradeAlert.eventID) {
        console.log(greyhoundRaceColl);
        greyhoundRace = await greyhoundRaceColl
          .find("_id like :_id")
          .bind("_id", inputTradeAlert.eventID)
          .execute();

        greyhoundRace = greyhoundRace.fetchOne();
      } else {
        greyhoundRace = await greyhoundRaceColl
          .find(`url like '${inputTradeAlert.eventURL}%'`)
          .execute();

        greyhoundRace = greyhoundRace.fetchOne();
      }
      console.log(greyhoundRace);
      if (!greyhoundRace) {
        await session.close();
        return false;
      }
      message = `
        Race: ${inputTradeAlert.event} ${new Date(
        (starttime + 3600) * 1000
      ).toISOString()}
        Strategy: ${inputTradeAlert.strategy}
        Selection: ${inputTradeAlert.selection}
        ${inputTradeAlert.odds ? `Odds: ${inputTradeAlert.odds}` : ""}
    `;
      trade = sportsTradeColl
        .find("eventID like :eventID AND sport like :sport AND userID like 0")
        .bind("eventID", greyhoundRace._id)
        .bind("sport", inputTradeAlert.sport);
      eventID = greyhoundRace._id;
    } else if (inputTradeAlert.sport === "Football") {
      message = `
        Match: ${inputTradeAlert.event} ${new Date(
        (starttime + 3600) * 1000
      ).toISOString()}
        Strategy: ${inputTradeAlert.strategy}
        Selection: ${inputTradeAlert.selection}
        ${inputTradeAlert.odds ? `Odds: ${inputTradeAlert.odds}` : ""}
    `;

      trade = sportsTradeColl
        .find("eventID like :eventID AND sport like :sport AND userID like 0")
        .bind("eventID", inputTradeAlert.eventID)
        .bind("sport", inputTradeAlert.sport);
      eventID = inputTradeAlert.eventID;

      stake = 1;
    }

    pubsub.publish("TRADE_ALERT", {
      tradeDetails: {
        strategy: inputTradeAlert.strategy,
        selection: inputTradeAlert.selection,
        odds: inputTradeAlert.odds ? inputTradeAlert.odds : 0,
        sport: inputTradeAlert.sport,
        event: inputTradeAlert.event,
        positionType: inputTradeAlert.positionType,
        eventID: eventID,
        starttime: starttime,
        difference: "",
      },
    });

    trade = await trade.execute();

    trade = trade.fetchOne();

    position = {
      runnerName: inputTradeAlert.selection,
      strategy: inputTradeAlert.strategy,
      positionType: inputTradeAlert.positionType,
      odds: inputTradeAlert.odds,
      stake: stake,
      discordMsg: message ? message : "",
    };
    if (!trade) {
      await sportsTradeColl
        .add({
          eventID: eventID,
          event: inputTradeAlert.event,
          sport: inputTradeAlert.sport,
          tradeDateTime: inputTradeAlert.starttime,
          positions: [position],
          userID: 0,
          newPosition: true,
        })
        .execute();
    } else {
      await sportsTradeColl
        .modify("_id like :tradeID")
        .bind("tradeID", trade._id)
        .set("positions", [...trade.positions, position])
        .set("newPosition", true)
        .execute();
    }
    await session.close();
    return true;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Adding Trade",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};

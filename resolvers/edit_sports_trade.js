const { connect } = require("../../../data/mysql/sql_connection");
const compilerSportTrade = require("../constants/compile_sports_trade");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
module.exports = async (
  _,
  { inputSportsTrade },
  { currentUser, horseDataLoader }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    let sportsTradeID;

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const sportsTradeColl = await mysqlDb.getCollection("sports_trades");
    console.log(sportsTradeColl);
    let sportsTrade = await sportsTradeColl
      .find("eventID like :eventID AND userID like :userID")
      .bind("eventID", inputSportsTrade.eventID)
      .bind("userID", currentUser)
      .execute();
    console.log(sportsTrade);
    sportsTrade = sportsTrade.fetchOne();
    console.log(sportsTrade);

    if (!sportsTrade) {
      sportsTradeID = await sportsTradeColl
        .add({
          ...inputSportsTrade,
          userID: currentUser,
        })
        .execute();

      sportsTradeID = sportsTradeID.getGeneratedIds()[0];
    } else {
      let { sentPositions, ...sentSportsTrade } = inputSportsTrade;

      let newSportTrade = {
        ...sportsTrade,
        ...sentSportsTrade,
        positions: [...sportsTrade.positions, ...sentPositions],
      };
      sportsTrade = await sportsTradeColl
        .find("eventID like :eventID AND userID like :userID")
        .bind("eventID", inputSportsTrade.eventID)
        .bind("userID", currentUser)
        .patch(newSportTrade)
        .execute();
      console.log(sportsTrade);

      sportsTradeID = sportsTrade.getGeneratedIds()[0];
    }

    sportsTrade = await sportsTradeColl
      .find("eventID like :eventID AND userID like :userID")
      .bind("eventID", inputSportsTrade.eventID)
      .bind("userID", currentUser)
      .execute();
    console.log(sportsTrade);
    sportsTrade = sportsTrade.fetchOne();
    console.log(sportsTrade);
    await session.close();
    return await compilerSportTrade(
      mysqlDb,
      sportsTradeID,
      sportsTrade,
      horseDataLoader
    );
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating Sports Event",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};

const compilerSportTrade = require("../constants/compile_sports_trade");
const mysqlx = require("@mysql/xdevapi");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
  DISCORD_MESSAGE,
} = require("../../../env.js");
const { pubsub } = require("../../../constants/pubsub");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
module.exports = async (
  _,
  { query },
  { currentUser, horseDataLoader, greyhoundDataLoader, teamDataLoader }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const sportsTradeColl = await mysqlDb.getCollection("sports_trades");
    console.log(sportsTradeColl);

    let sportsTrades = await sportsTradeColl.find(query).execute();

    console.log(sportsTrades);
    sportsTrades = sportsTrades.fetchAll();
    console.log(sportsTrades);
    await session.close();
    return sportsTrades;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Getting Sports Event",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
